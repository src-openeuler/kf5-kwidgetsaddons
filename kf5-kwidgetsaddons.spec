%undefine __cmake_in_source_build
%global framework kwidgetsaddons

Name:           kf5-%{framework}
Version:        5.116.0
Release:        1
Summary:        KDE Frameworks 5 Tier 1 addon with various classes on top of QtWidgets

License:        CC0-1.0 AND GPL-2.0-or-later AND LGPL-2.0-only AND LGPL-2.0-or-later AND LGPL-2.1-only AND LGPL-2.1-or-later AND LGPL-3.0-only AND LGPL-3.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/frameworks/%{majmin}/%{framework}-%{version}.tar.xz

BuildRequires:  extra-cmake-modules >= %{majmin}
BuildRequires:  kf5-rpm-macros
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qttools-devel
BuildRequires:  qt5-qttools-static
BuildRequires:  pkgconfig(xkbcommon)

Requires:       kf5-filesystem >= %{majmin}

%description
KDE Frameworks 5 Tier 1 addon with various classes on top of QtWidgets.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       qt5-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version}


%build
%{cmake_kf5}

%cmake_build


%install
%cmake_install

%find_lang_kf5 kwidgetsaddons5_qt

%files -f kwidgetsaddons5_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf5_libdir}/libKF5WidgetsAddons.so.*
%{_kf5_datadir}/kf5/kcharselect/
%{_kf5_datadir}/qlogging-categories5/*categories

%files devel

%{_kf5_includedir}/KWidgetsAddons/
%{_kf5_libdir}/libKF5WidgetsAddons.so
%{_kf5_libdir}/cmake/KF5WidgetsAddons/
%{_kf5_archdatadir}/mkspecs/modules/qt_KWidgetsAddons.pri
%{_kf5_qtplugindir}/designer/kwidgetsaddons5widgets.so


%changelog
* Mon Nov 25 2024 peijiankang <peijiankang@kylinos.cn> - 5.116.0-1
- Upgrade package to 5.116.0

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 5.115.0-2
- adapt to the new CMake macros to fix build failure

* Fri Mar 01 2024 maqi <maqi@uniontech.com> - 5.115.0-1
- Update package to version 5.115.0

* Tue Jan 02 2024 wangqia <wangqia@uniontech.com> - 5.113.0-1
- Update to upstream version 5.113.0

* Thu Aug 03 2023 peijiankang <peijiankang@kylinos.cn> - 5.108.0-1
- update to upstream version 5.108.0

* Mon Dec 12 2022 lijian <lijian2@kylinos.cn> - 5.100.0-1
- update to upstream version 5.100.0

* Mon Sep 05 2022 liweiganga <liweiganga@uniontech.com> - 5.97.0-1
- update to upstream version 5.97.0

* Tue Jul 05 2022 loong_C <loong_c@yeah.net> - 5.95.0-1
- update to upstream version 5.95.0

* Sat Feb 12 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.90.0-1
- update to upstream version 5.90.0

* Fri Jan 14 2022 pei-jiankang <peijiankang@kylinos.cn> - 5.88.0-1
- update to upstream version 5.88.0

* Mon Dec 13 2021 wangqing <wangqing@uniontech.com> - 5.55.0-2
- delete %dist

* Thu Jul 23 2020 wangmian <wangmian@kylinos.cn> - 5.55.0-1
- Init kf5-kwidgetsaddons project
